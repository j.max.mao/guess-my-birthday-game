from cmd import PROMPT
from random import randint

name = input('Hi! What is your name? ')

for guess_number in range(1,6):

    month = randint(1, 12)
    year = randint(1924, 2004)

    print('Guess', guess_number, 'were you born in', 
            month,'/', year, '?')

    response = input('yes or no? ')

    if response == 'yes':
        print('I knew it!')
        exit()
    
    elif (guess_number == 5):
        print('Darn! I give up!')
    else:
        print("Oh no! Let me try again.")

